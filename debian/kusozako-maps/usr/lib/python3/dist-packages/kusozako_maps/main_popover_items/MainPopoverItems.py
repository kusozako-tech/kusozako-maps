
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .models.Models import DeltaModels


class DeltaMainPopoverItems(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        DeltaModels(self)
