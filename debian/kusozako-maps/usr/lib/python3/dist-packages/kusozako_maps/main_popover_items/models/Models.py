
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .SwitcherItem import SWITCHER_ITEM
from .MapLayerPage import MAP_LAYER_PAGE


class DeltaModels(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        self._raise(
            "delta > application popover add item",
            ("main", SWITCHER_ITEM)
            )
        self._raise("delta > application popover add page",
            MAP_LAYER_PAGE
            )
