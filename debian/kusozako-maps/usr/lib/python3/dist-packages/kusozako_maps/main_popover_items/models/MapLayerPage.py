
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_maps import MapLayers


MAP_LAYER_PAGE = {
    "page-name": "map-layer",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "check-action",
            "title": _("Open Street Map"),
            "message": "delta > settings",
            "user-data": ("map", "layer", MapLayers.OPEN_STREET_MAP),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("map", "layer", MapLayers.OPEN_STREET_MAP),
            "check-value": MapLayers.OPEN_STREET_MAP
        },
        {
            "type": "check-action",
            "title": _("Google Map"),
            "message": "delta > settings",
            "user-data": ("map", "layer", MapLayers.GOOGLE_MAP),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("map", "layer", MapLayers.OPEN_STREET_MAP),
            "check-value": MapLayers.GOOGLE_MAP
        },
        {
            "type": "check-action",
            "title": _("Google Earth"),
            "message": "delta > settings",
            "user-data": ("map", "layer", MapLayers.GOOGLE_EARTH),
            "close-on-clicked": False,
            "registration": "delta > register settings object",
            "query": "delta > settings",
            "query-data": ("map", "layer", MapLayers.OPEN_STREET_MAP),
            "check-value": MapLayers.GOOGLE_EARTH
        },
    ]
}
