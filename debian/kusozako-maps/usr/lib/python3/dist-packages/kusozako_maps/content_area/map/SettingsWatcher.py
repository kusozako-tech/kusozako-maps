
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class DeltaSettingsWatcher(DeltaEntity):

    def receive_transmission(self, user_data):
        group, key, layer_id = user_data
        if group != "map" or key != "layer":
            return
        self._raise("delta > map layer changed", layer_id)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register settings object", self)
