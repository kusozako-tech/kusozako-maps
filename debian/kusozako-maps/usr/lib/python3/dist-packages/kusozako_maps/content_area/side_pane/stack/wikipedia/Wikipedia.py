
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import SidePanePageNames
from .articles.Articles import DeltaArticles
from .viewer.Viewer import DeltaViewer


class DeltaWikipedia(Gtk.Stack, DeltaEntity):

    def _delta_call_switch_stack_to(self, page_name):
        self.set_visible_child_name(page_name)

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self, hexpand=True)
        DeltaArticles(self)
        DeltaViewer(self)
        data = self, SidePanePageNames.WIKIPEDIA
        self._raise("delta > add to stack named", data)
