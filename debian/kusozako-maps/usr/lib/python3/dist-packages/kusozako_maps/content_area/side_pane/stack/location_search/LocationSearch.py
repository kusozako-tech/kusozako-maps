
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_maps import SidePanePageNames
from .model.Model import DeltaModel
from .widgets.Widgets import EchoWidgets


class DeltaLocationSearch(Gtk.Box, DeltaEntity):

    def _delta_info_model(self):
        return self._model

    def _delta_call_search_location(self, query):
        self._model.search_location(query)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=Unit(2)
            )
        self._model = DeltaModel(self)
        EchoWidgets(self)
        param = self, SidePanePageNames.LOCATION_SEARCH
        self._raise("delta > add to stack named", param)
