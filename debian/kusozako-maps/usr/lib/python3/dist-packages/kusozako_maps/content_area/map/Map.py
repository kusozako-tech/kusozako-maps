
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import OsmGpsMap
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals
from kusozako_maps import WikipediaColumns
from .MapOsd import DeltaMapOsd
from .SettingsWatcher import DeltaSettingsWatcher

DEFAULT_SETTINGS = ("map", "layer", 1)


class DeltaMap(OsmGpsMap.Map, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, user_data = user_data
        if signal == InterModelSignals.LOCATION_CHANGED:
            location, zoom = user_data
            latitude = location.get_latitude()
            longitude = location.get_longitude()
            self.set_center_and_zoom(latitude, longitude, zoom)
        elif signal == InterModelSignals.WIKIPEDIA_ARTICELE_SELECTED:
            tree_row = user_data
            latitude = tree_row[WikipediaColumns.LATITUDE]
            longitude = tree_row[WikipediaColumns.LONGITUDE]
            self.set_center_and_zoom(latitude, longitude, 16)
            title = tree_row[WikipediaColumns.TITLE]
            self._raise("delta > application window title", title)

    def _delta_call_map_layer_changed(self, layer_id):
        self.set_property("map-source", layer_id)

    def __init__(self, parent):
        self._parent = parent
        OsmGpsMap.Map.__init__(self)
        settings = self._enquiry("delta > settings", DEFAULT_SETTINGS)
        self.set_property("map-source", settings)
        self.layer_add(DeltaMapOsd(self))
        self._raise("delta > add to container", self)
        self._raise("delta > register inter model object", self)
        DeltaSettingsWatcher(self)
