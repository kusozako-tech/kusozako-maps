
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import WeatherModelColumns
from kusozako_maps import InterModelSignals
from .MetWeatherApi import DeltaMetWeatherApi
from .RowDataFactory import DeltaRowDataFactory


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append_tree_row_data(self, tree_row_data):
        self.append(tree_row_data)

    def _delta_call_soup_session_finished(self, json_dict):
        self.clear()
        for time_series in json_dict["properties"]["timeseries"]:
            self._row_data_factory.construct_data(self._timezone, time_series)
        self._raise("delta > switch main stack to", "weather")

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != InterModelSignals.LOCATION_CHANGED:
            return
        location, _ = param
        self._timezone = GLib.TimeZone.new(location.get_zone())
        query = location.get_latitude(), location.get_longitude()
        self._weather_api.search(query)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *WeatherModelColumns.TYPES)
        self._row_data_factory = DeltaRowDataFactory(self)
        self._weather_api = DeltaMetWeatherApi(self)
        self._raise("delta > register inter model object", self)
