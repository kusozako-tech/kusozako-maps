
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .stack_switch.StackSwitch import DeltaStackSwitch
from .stack.Stack import DeltaStack


class DeltaSidePane(Gtk.Box, DeltaEntity):

    def _delta_call_switch_stack_to(self, page_name):
        self._stack.set_visible_child_name(page_name)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=Unit(2)
            )
        DeltaStackSwitch(self)
        self._stack = DeltaStack(self)
        self._raise("delta > add to container", self)
