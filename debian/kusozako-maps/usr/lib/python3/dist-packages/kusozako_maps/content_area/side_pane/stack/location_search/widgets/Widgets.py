
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TimezoneMap import DeltaTimezoneMap
from .SearchEntry import DeltaSearchEntry
from .tree_view.TreeView import DeltaTreeView


class EchoWidgets:

    def __init__(self, parent):
        timezone_map = DeltaTimezoneMap(parent)
        DeltaSearchEntry(parent)
        DeltaTreeView(parent)
