
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import TimezoneMap
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals


class DeltaTimezoneMap(TimezoneMap.TimezoneMap, DeltaEntity):

    def _on_location_changed(self, timezone_map, location):
        application_name = self._enquiry("delta > application data", "name")
        self._raise("delta > application window title", application_name)
        param = InterModelSignals.LOCATION_CHANGED, (location, 8)
        self._raise("delta > inter model signal", param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != InterModelSignals.LOCATION_SELECTED:
            return
        latitude, longitude = param
        GObject.signal_handler_block(self, self._id)
        self.clear_location()
        self.set_location(longitude, latitude)
        location = self.get_location()
        location.set_latitude(latitude)
        location.set_longitude(longitude)
        param = InterModelSignals.LOCATION_CHANGED, (location, 14)
        self._raise("delta > inter model signal", param)
        GObject.signal_handler_unblock(self, self._id)

    def __init__(self, parent):
        self._parent = parent
        TimezoneMap.TimezoneMap.__init__(self)
        self._id = self.connect("location-changed", self._on_location_changed)
        self._raise("delta > add to container", self)
        self._raise("delta > register inter model object", self)
