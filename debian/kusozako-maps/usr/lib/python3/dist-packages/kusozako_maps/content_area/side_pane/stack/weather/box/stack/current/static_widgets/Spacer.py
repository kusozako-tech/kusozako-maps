
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaSpacer(Gtk.Label, DeltaEntity):

    @classmethod
    def new_for_geometries(cls, parent, geometries):
        spacer = cls(parent)
        spacer.set_geometries(geometries)

    def set_geometries(self, geometries):
        self._raise("delta > attach to grid", (self, geometries))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, hexpand=True, vexpand=True)
