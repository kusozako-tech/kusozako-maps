
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit

MESSAGE = ("forecast", "current")
LABEL = (_("Show Forecast"), _("Close Forecast"))


class DeltaButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", MESSAGE[self._index])
        self._index = ((self._index+1) % 2)
        self.set_label(LABEL[self._index])

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        Gtk.Button.__init__(self, LABEL[self._index])
        self.set_size_request(-1, Unit(4))
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
