
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import LocationColumns
from .OsmNominatim import DeltaOsmNominatim

TOOLTIP = "{}\nLatitude: {}\nLongitude: {}"


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_model_data(self, model_data):
        for node in model_data:
            type_ = node["type"]
            # print(type_)
            display_name = node["display_name"]
            latitude = float(node["lat"])
            longitude = float(node["lon"])
            tooltip = TOOLTIP.format(display_name, latitude, longitude)
            self.append((type_, display_name, latitude, longitude, tooltip))

    def search_location(self, query):
        self.clear()
        self._osm_nominatim.search(query)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *LocationColumns.TYPES)
        self._osm_nominatim = DeltaOsmNominatim(self)
