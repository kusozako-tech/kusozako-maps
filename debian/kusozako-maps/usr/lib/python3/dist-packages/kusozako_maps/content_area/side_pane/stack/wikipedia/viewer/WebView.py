
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import WebKit2
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals
from kusozako_maps import WikipediaColumns

URI_TEMPLATE = "https://en.m.wikipedia.org/wiki?curid={}"


class DeltaWebView(WebKit2.WebView, DeltaEntity):

    def _on_load_changed(self, web_view, load_event):
        if load_event == WebKit2.LoadEvent.FINISHED:
            self._raise("delta > switch stack to", "viewer")

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal != InterModelSignals.WIKIPEDIA_ARTICELE_SELECTED:
            return
        page_id = tree_row[WikipediaColumns.PAGE_ID]
        uri = URI_TEMPLATE.format(page_id)
        self.load_uri(uri)

    def __init__(self, parent):
        self._parent = parent
        WebKit2.WebView.__init__(self, vexpand=True)
        self.connect("load-changed", self._on_load_changed)
        self._settings = WebKit2.Settings()
        self._settings.set_enable_javascript(True)
        self.set_settings(self._settings)
        self._raise("delta > register inter model object", self)
        self._raise("delta > add to container", self)
