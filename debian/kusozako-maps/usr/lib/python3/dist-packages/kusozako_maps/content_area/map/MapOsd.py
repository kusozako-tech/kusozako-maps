
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import OsmGpsMap
from libkusozako3.Entity import DeltaEntity


class DeltaMapOsd(DeltaEntity, OsmGpsMap.MapOsd):

    def __init__(self, parent):
        self._parent = parent
        OsmGpsMap.MapOsd.__init__(self)
        self.set_property("show-scale", True)
        self.set_property("show-zoom", True)
        self.set_property("show-dpad", True)
