
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Label import DeltaLabel
from .Spacer import DeltaSpacer

LABEL_MODELS = [
    (_("Temperature :"), (0, 4, 1, 1)),
    (_("Cloudiness :"), (0, 5, 1, 1)),
    (_("Precipitation :"), (0, 6, 1, 1)),
    (_("Humidity :"), (0, 7, 1, 1)),
    (_("Air Pressure :"), (0, 8, 1, 1)),
    (_("Window Direction :"), (0, 9, 1, 1)),
    (_("Window Speed :"), (0, 10, 1, 1)),
    ]


class EchoStaticWidgets:

    def __init__(self, parent):
        DeltaSpacer.new_for_geometries(parent, (0, -1, 2, 1))
        DeltaSpacer.new_for_geometries(parent, (0, 3, 2, 1))
        for label_model in LABEL_MODELS:
            DeltaLabel.new_for_model(parent, label_model)
        DeltaSpacer.new_for_geometries(parent, (0, 11, 2, 1))
