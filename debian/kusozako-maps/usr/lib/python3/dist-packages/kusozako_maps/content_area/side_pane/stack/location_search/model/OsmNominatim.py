
import xml.etree.ElementTree as XmlParser
from kusozako_maps.api.Api import AlfaApi

API = "https://nominatim.openstreetmap.org/search?q={}&format=xml"


class DeltaOsmNominatim(AlfaApi):

    def _delta_info_soup_session_user_agent(self):
        return "kusozako-maps"

    def _get_nodes(self, xml_string):
        return XmlParser.fromstring(xml_string).iter("place")

    def _get_api(self, query):
        return API.format(query)
