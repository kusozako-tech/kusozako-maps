
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .current.Current import DeltaCurrent
from .forecast.Forecast import DeltaForecast


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.OVER_UP_DOWN
            )
        DeltaCurrent(self)
        DeltaForecast(self)
        self._raise("delta > add to container", self)
