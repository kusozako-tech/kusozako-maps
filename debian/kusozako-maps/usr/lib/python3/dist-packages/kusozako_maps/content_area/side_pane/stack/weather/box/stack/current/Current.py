
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .static_widgets.StaticWidgets import EchoStaticWidgets
from .data_widgets.DataWidgets import EchoDataWidgets


class DeltaCurrent(Gtk.Grid, DeltaEntity):

    def _delta_call_attach_to_grid(self, user_data):
        widget, geometries = user_data
        self.attach(widget, *geometries)

    def _on_map(self, *args):
        model = self._enquiry("delta > current model")
        if model is not None:
            self._data_widgets.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Grid.__init__(self, row_spacing=Unit(1), column_spacing=Unit(1))
        EchoStaticWidgets(self)
        self._data_widgets = EchoDataWidgets(self)
        self.connect("map", self._on_map)
        self._raise("delta > add to stack named", (self, "current"))
        self._raise("delta > css", (self, "primary-surface-color-class"))
