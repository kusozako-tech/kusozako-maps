
import xml.etree.ElementTree as XmlParser
from kusozako_maps.api.Api import AlfaApi

API = \
    "https://{}.wikipedia.org/w/api.php?action=query"\
    "&format=xml"\
    "&list=geosearch"\
    "&gslimit=100"\
    "&gsradius=10000"\
    "&gscoord={}|{}"


class DeltaApi(AlfaApi):

    def _delta_info_soup_session_user_agent(self):
        return "kusozako-maps"

    def _get_nodes(self, xml_string):
        return XmlParser.fromstring(xml_string).iter("gs")

    def _get_api(self, query):
        return API.format("en", *query)
