
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals
from kusozako_maps import LocationColumns
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_insert_column(self, column):
        self.append_column(column)

    def _delta_info_tree_view(self):
        return self

    def _on_row_activated(self, tree_view, path, column):
        model = tree_view.get_model()
        tree_row = model[path]
        display_name = tree_row[LocationColumns.DISPLAY_NAME]
        self._raise("delta > application window title", display_name)
        latitude = tree_row[LocationColumns.LATITUDE]
        longitude = tree_row[LocationColumns.LONGITUDE]
        signal_param = latitude, longitude
        param = InterModelSignals.LOCATION_SELECTED, signal_param
        self._raise("delta > inter model signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TreeView.__init__(
            self,
            vexpand=True,
            hexpand=True,
            model=self._enquiry("delta > model")
            )
        self.connect("row-activated", self._on_row_activated)
        self.set_tooltip_column(LocationColumns.TOOLTIP)
        EchoColumns(self)
        self._raise("delta > add to container", self)
