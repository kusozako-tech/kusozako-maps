
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class AlfaButton(Gtk.Button, DeltaEntity):

    LABEL = "define label here."
    PAGE_NAME = "define page name to switch here."

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", self.PAGE_NAME)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            self.LABEL,
            relief=Gtk.ReliefStyle.NONE,
            hexpand=True
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
