
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi

gi.require_version('TimezoneMap', '1.0')
gi.require_version('OsmGpsMap', '1.0')

VERSION = "2022.10.14"
APPLICATION_NAME = "kusozako-maps"
GROUP_ID = "com.gitlab.kusozako-tech"
APPLICATION_ID = "{}.{}".format(GROUP_ID, APPLICATION_NAME)

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

LONG_DESCRIPTION = _("""Open Street Map Viewer for kusozako project.
This software is licencced under GPL version 3 or any later version.""")

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": "Open Street Map Viewer",
    "long-description": LONG_DESCRIPTION
    }
