
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

LOCATION_SELECTED = 0       # (float, float) as (latitude, longitude)
LOCATION_CHANGED = 1        # (object, int) as (TimezoneLocation, zoom_rate)
WIKIPEDIA_ARTICELE_SELECTED = 2     # tree_row for WikipediaColumns
