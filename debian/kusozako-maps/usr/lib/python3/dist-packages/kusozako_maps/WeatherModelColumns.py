
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

GLIB_DATE_TIME = 0
READABLE_DATE_TIME = 1
AIR_PRESSURE_AT_SEA_LEVEL = 2
AIR_TEMPERATURE = 3
CLOUD_AREA_FRACTION = 4
RELATIVE_HUMIDITY = 5
WIND_FROM_DIRECTION = 6
WIND_SPEED = 7
SYMBOL_CODE = 8
PRECIPITATION_AMOUNT = 9
DESCRIPTION = 10
NUMBER_OF_COLUMNS = 11


TYPES = (
    GLib.DateTime,  # date time
    str,            # readable date time
    float,          # air pressure at sea level
    float,          # air temperature
    float,          # cloud area fraction
    float,          # relative humidity
    float,          # wind from direction
    float,          # wind sped
    str,            # symbol code
    float,          # precipitation amount
    str,            # 10 : DESCRIPTION for tmperature and humidity
    )
