
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .CommandLine import OPTIONS
from .content_area.ContentArea import DeltaContentArea
from .main_popover_items.MainPopoverItems import DeltaMainPopoverItems


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_main_window_ready(self, parent):
        DeltaMainPopoverItems(parent)
        DeltaContentArea(parent)

    def _delta_info_command_line_options(self):
        return OPTIONS

    def _delta_info_application_data(self, key):
        return APPLICATION_DATA.get(key, None)

    def _delta_info_application_library_directory(self):
        return GLib.path_get_dirname(__file__)
