
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

LOCATION_SEARCH = "location-search"
WEATHER = "weather"
WIKIPEDIA = "wikipedia"
