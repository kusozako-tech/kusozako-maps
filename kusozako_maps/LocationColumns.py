
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later


LOCATION_TYPE = 0
DISPLAY_NAME = 1
LATITUDE = 2
LONGITUDE = 3
TOOLTIP = 4

TYPES = (
    str,            # LOCATION_TYPE
    str,            # DISPLAY_NAME
    float,          # LATITUDE
    float,          # LONGITUDE
    str             # TOOLTIP
    )
