
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


OPTIONS = [
    (
        "version",
        ord("V"),
        GLib.OptionFlags.NONE,
        GLib.OptionArg.NONE,
        _("show version"),
        None
    )
]
