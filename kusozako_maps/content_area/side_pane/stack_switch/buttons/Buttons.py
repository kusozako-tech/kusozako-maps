
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SearchLocation import DeltaSearchLocation
from .Weather import DeltaWeather
from .Wikipedia import DeltaWikipedia


class EchoButtons:

    def __init__(self, parent):
        DeltaSearchLocation(parent)
        DeltaWeather(parent)
        DeltaWikipedia(parent)
