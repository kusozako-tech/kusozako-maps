
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_maps import SidePanePageNames
from .Button import AlfaButton


class DeltaWikipedia(AlfaButton):

    LABEL = "Wikipedia"
    PAGE_NAME = SidePanePageNames.WIKIPEDIA
