
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_maps import SidePanePageNames
from .Button import AlfaButton


class DeltaSearchLocation(AlfaButton):

    LABEL = "search"
    PAGE_NAME = SidePanePageNames.LOCATION_SEARCH
