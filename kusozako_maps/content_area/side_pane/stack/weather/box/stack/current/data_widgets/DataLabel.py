
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaDataLabel(Gtk.Label, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        label = cls(parent)
        label.initialize(model)
        return label

    def set_model(self, model):
        label = self._template.format(model[self._key])
        self.set_label(label)

    def initialize(self, label_model):
        self._template, self._key, geometries = label_model
        self._raise("delta > attach to grid", (self, geometries))

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, hexpand=True)
        self.set_xalign(0)
