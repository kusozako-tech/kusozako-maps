
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import WeatherModelColumns


class DeltaConditionLabel(Gtk.Label, DeltaEntity):

    def set_model(self, tree_row):
        self.set_label(tree_row[WeatherModelColumns.SYMBOL_CODE])

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, hexpand=True)
        geometries = 0, 1, 2, 1
        self._raise("delta > attach to grid", (self, geometries))
