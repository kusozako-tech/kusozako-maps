
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_maps import WeatherModelColumns
from .WeatherIcon import DeltaWeatherIcon
from .ConditionLabel import DeltaConditionLabel
from .LocationLabel import DeltaLocationLabel
from .DataLabel import DeltaDataLabel

DATA_LABEL_MODELS = [
    ("{} ℃", WeatherModelColumns.AIR_TEMPERATURE, (1, 4, 1, 1)),
    ("{} %", WeatherModelColumns.CLOUD_AREA_FRACTION, (1, 5, 1, 1)),
    ("{} mm", WeatherModelColumns.PRECIPITATION_AMOUNT, (1, 6, 1, 1)),
    ("{} %", WeatherModelColumns.RELATIVE_HUMIDITY, (1, 7, 1, 1)),
    ("{} hPa", WeatherModelColumns.AIR_PRESSURE_AT_SEA_LEVEL, (1, 8, 1, 1)),
    ("{}", WeatherModelColumns.WIND_FROM_DIRECTION, (1, 9, 1, 1)),
    ("{} km/h", WeatherModelColumns.WIND_SPEED, (1, 10, 1, 1))
    ]


class EchoDataWidgets:

    def set_model(self, model):
        for widget in self._widgets:
            widget.set_model(model)

    def __init__(self, parent):
        self._widgets = []
        self._widgets.append(DeltaWeatherIcon(parent))
        self._widgets.append(DeltaConditionLabel(parent))
        self._widgets.append(DeltaLocationLabel(parent))
        for data_label_model in DATA_LABEL_MODELS:
            data_label = DeltaDataLabel.new_for_model(parent, data_label_model)
            self._widgets.append(data_label)
