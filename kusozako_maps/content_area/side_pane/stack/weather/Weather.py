
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .model.Model import DeltaModel
from .box.Box import DeltaBox


class DeltaWeather(DeltaEntity):

    def _delta_info_current_model(self):
        return self._model[0] if self._model else None

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        DeltaBox(self)
