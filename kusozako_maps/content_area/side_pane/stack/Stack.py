
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .location_search.LocationSearch import DeltaLocationSearch
from .weather.Weather import DeltaWeather
from .wikipedia.Wikipedia import DeltaWikipedia


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        DeltaLocationSearch(self)
        DeltaWeather(self)
        DeltaWikipedia(self)
        self._raise("delta > add to container", self)
