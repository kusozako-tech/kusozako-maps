
from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_maps import LocationSearchColumns


class DeltaTypeColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, tree_iter, user_data):
        pass

    def _delta_info_tree_view_column(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(
            xpad=Unit(1),
            # ellipsize=Pango.EllipsizeMode.END,
            xalign=0,
            )
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            title=_("Type"),
            text=LocationSearchColumns.LOCATION_TYPE,
            # expand=False
            )
        self.set_sort_column_id(LocationSearchColumns.LOCATION_TYPE)
        self.set_expand(False)
        # DeltaColumnSorter(self)
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func)
