
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals
from kusozako_maps import WikipediaColumns
from .Api import DeltaApi


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_model_data(self, model_data):
        for article in model_data:
            row_data = (
                int(article["pageid"]),
                article["title"],
                float(article["lat"]),
                float(article["lon"])
                )
            self.append(row_data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != InterModelSignals.LOCATION_CHANGED:
            return
        self.clear()
        location, _ = param
        query = location.get_latitude(), location.get_longitude()
        self._api.search(query)

    def __init__(self, parent):
        self._parent = parent
        self._api = DeltaApi(self)
        Gtk.ListStore.__init__(self, *WikipediaColumns.TYPES)
        self._raise("delta > register inter model object", self)
