
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_maps import InterModelSignals
from .Column import DeltaColumn


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _on_row_activated(self, tree_view, tree_path, column):
        model = tree_view.get_model()
        tree_row = model[tree_path]
        param = InterModelSignals.WIKIPEDIA_ARTICELE_SELECTED, tree_row
        self._raise("delta > inter model signal", param)

    def _delta_call_append_column(self, column):
        self.append_column(column)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        self.connect("row-activated", self._on_row_activated)
        DeltaColumn(self)
        scrolled_window.add(self)
        data = scrolled_window, "tree-view"
        self._raise("delta > add to stack named", data)
