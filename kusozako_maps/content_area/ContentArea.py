
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from .side_pane.SidePane import DeltaSidePane
from .map.Map import DeltaMap


class DeltaContentArea(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_inter_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_inter_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Gtk.Paned.__init__(self, wide_handle=True)
        DeltaSidePane(self)
        DeltaMap(self)
        self._raise("delta > add to container", self)
