
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PAGE_ID = 0
TITLE = 1
LATITUDE = 2
LONGITUDE = 3
NUMBER_OF_COLUMNS = 4


TYPES = (
    int,            # PAGE_ID
    str,            # TITLE
    float,          # LATITUDE
    float           # LONGITUDE
    )
